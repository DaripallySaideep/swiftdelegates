//
//  DeleteViewController.swift
//  SwiftBasics
//
//  Created by Kvana Inc 1 on 14/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

protocol DeleteDelegate {
    func deleteNames()
}

class DeleteViewController: UIViewController {
     var delegateCustom : DeleteDelegate?
    
   override func viewDidLoad() {
        super.viewDidLoad()

          }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
         }
    @IBAction func deleteBtnTapped(sender: AnyObject) {
         self.delegateCustom?.deleteNames()
        self.navigationController!.popViewControllerAnimated(true)
    }
    
}
