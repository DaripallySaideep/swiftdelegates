//
//  ViewController.swift
//  SwiftBasics
//
//  Created by Kvana Inc 1 on 14/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,DeleteDelegate{
    

    @IBOutlet weak var namesTable: UITableView!
    var namesArray = [String] ()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        namesArray = ["siadeep","sandeep","ravinath","vinodh","sagar","sarfarosh","savithri","rashmi","anasuya"]
        self.namesTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
           }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return namesArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = self.namesTable.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
        cell.textLabel?.text = namesArray[indexPath.row]
        return cell
        }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let dvc = self.storyboard!.instantiateViewControllerWithIdentifier("DeleteViewController") as! DeleteViewController
        dvc.delegateCustom = self
    self.navigationController!.pushViewController(dvc, animated: true)

    }
    func deleteNames() {
           namesArray.removeAtIndex(0)
        self.namesTable.reloadData()
    }
}

