//: Playground - noun: a place where people can play

import UIKit
import Foundation

protocol Speaker {
    func Speak()
}

protocol Listener {
    func Listen()
}

@objc protocol Player {
  optional func Play()
}

class Names{
    
}

class  NickNames{

}

// cannpt inherit with two classes(i.e multiple inheritence is not allowed)
// where as multiple protocalls can be inherited
//class Try:Names,NickNames{
//    
//}



class Calci : Names, Speaker,Listener,Player{
    let total:Int
    let first:Int
    let second:Int
    
    
    init(first:Int , second:Int){
        self.first = first
        self.second = second
        total = first+second
    }
    
    func calcAmount(amount : Int) -> Int {
        return total * amount
    }
    func printPossibleAmount(){
        print("\(calcAmount(10))")
        print("\(calcAmount(20))")
        print("\(calcAmount(30))")
    }
    func Speak() {
        print("speakproto")
    }
    func Listen() {
         print("listenproto")
    }
    }

let calci = Calci(first: 10, second: 20)
calci.printPossibleAmount()

